import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Lesson } from './interfaces/lesson.interface';
import { v4 as uuid } from 'uuid'
import { CreateLessonInput } from './lesson.input';

@Injectable()
export class LessonService {
    constructor(
        @InjectModel('Lesson') private readonly lessonModel: Model<Lesson>,
    ) {}

    async getLesson(id: string): Promise<Lesson> {
        return this.lessonModel.findOne({ id })
    }

    async getAllLesson(): Promise<Lesson[]> {
        return this.lessonModel.find()
    }

    async createLesson(createLessonInput: CreateLessonInput): Promise<Lesson> {
        const { name, startDate, endDate } = createLessonInput

        const lesson = new this.lessonModel({
            id: uuid(),
            name,
            startDate,
            endDate
        })
        
        return await lesson.save()
    }

}
