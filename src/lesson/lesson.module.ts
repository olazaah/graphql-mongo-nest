import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { LessonResolver } from './lesson.resolver';
import { LessonService } from './lesson.service';
import { LessonSchema } from './schemas/lesson.schema';

@Module({
    imports: [
        MongooseModule.forFeature([{ name: 'Lesson', schema: LessonSchema }])
    ],
    providers: [LessonService, LessonResolver]
})
export class LessonModule {}
