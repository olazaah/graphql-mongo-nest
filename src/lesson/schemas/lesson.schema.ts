import * as mongoose from 'mongoose'

export const LessonSchema = new mongoose.Schema({
    id: String,
    name: String,
    startDate: String,
    endDate: String,
})